## Test CG openvpn

This repo contains code/info that helps you connect to CG OpenVPN via container. I used arch since it gives me the last version of docker without any stress, but any other distro can be used.

Use the following Dockerfile to create your docker image for OpenVPN:
```
FROM alpine:edge
RUN apk add --no-cache openvpn
CMD ["openvpn /root/openvpn.ovpn"]
```

Log into the CG portal and download your OpenVPN config for Linux. Unzip it into a FOLDER and also add the following script (called start.sh) into the FOLDER:
```
mkdir -p /dev/net
mknod /dev/net/tun c 10 200
chmod 666 /dev/net/tun
openvpn /root/openvpn.ovpn
```

Now, if you carefully read what the script does, you probably figured you have to mount the FOLDER into /root/ into the contianer. You also need to start the container with NET_ADMIN capabilities and use --sysctl to enable IPv6, so your run command will look as follows:
```
docker run -dv /path/to/your/FOLDER/:/root/ --cap-add=NET_ADMIN --sysctl net.ipv6.conf.all.disable_ipv6=0 cg/openvpn-client /bin/sh /root/start.sh
```
If you did it right, you can run it multiple times since the default DNS name within the config will point to multiple IPs. Of course, after 3 connections you will start to get rejected AUTH. It should look like this:
```
 INSERT   dockerHost  root  ~  docker stats --no-stream 
CONTAINER ID        NAME                    CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
2bf38141263e        compassionate_keldysh   0.00%               1.934MiB / 9.576GiB   0.02%               23kB / 33kB         5.21MB / 0B         2
d26072ddab0c        cranky_banach           0.00%               2.023MiB / 9.576GiB   0.02%               26.4kB / 34.8kB     5.21MB / 0B         2
e7cfc5afc7c7        reverent_spence         0.03%               2.094MiB / 9.576GiB   0.02%               27kB / 35kB         5.21MB / 0B         2
```

## Next steps
Use this method to spawn 300 containers. But first, create the 300 FOLDERs, with 300 separate configs, one for each instance. 
The contianers that have connection issues will kill the openvpn process, so monitoring can be done counting the number of opened containers, to begin with.

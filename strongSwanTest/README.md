# Simple strongswan test using archlinux

I used arch since it has all packages up to date.

In order to run the setup:

```
vagrant up
cd ansible
ansible-playbook -i inventory.ini  deploy.yaml
```

You can check if the config is working by logging into any VM and running `ip xfrm state`.

The private keys are injected, NOT generated, since that is not part of the scope of this test. If you want to create your own keys/certs, refer to official documentation: https://wiki.strongswan.org/projects/strongswan/wiki/SimpleCA

Expected output:

```
 INSERT  gunix  …  vagrant-files  strongSwanTest  ansible  ansible-playbook -i inventory.ini  deploy.yaml

PLAY [all] ***********************************************************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************
ok: [10.10.10.102]
ok: [10.10.10.101]

TASK [strongswan : do a full upgrade] ********************************************************************************************************************************************************
changed: [10.10.10.101]
changed: [10.10.10.102]

TASK [strongswan : Reboot the system since there was an upgrade] *****************************************************************************************************************************
changed: [10.10.10.102]
changed: [10.10.10.101]

TASK [strongswan : Wait for the reboot to complete] ******************************************************************************************************************************************
ok: [10.10.10.101]
ok: [10.10.10.102]

TASK [strongswan : install strongswan] *******************************************************************************************************************************************************
changed: [10.10.10.102]
changed: [10.10.10.101]

TASK [strongswan : Install CA Certificate] ***************************************************************************************************************************************************
changed: [10.10.10.102]
changed: [10.10.10.101]

TASK [strongswan : Install ipsec.conf] *******************************************************************************************************************************************************
changed: [10.10.10.102]
changed: [10.10.10.101]

TASK [strongswan : Install ipsec.secrets] ****************************************************************************************************************************************************
changed: [10.10.10.101]
changed: [10.10.10.102]

TASK [strongswan : Install certificate] ******************************************************************************************************************************************************
changed: [10.10.10.101]
changed: [10.10.10.102]

TASK [strongswan : Install private key] ******************************************************************************************************************************************************
changed: [10.10.10.101]
changed: [10.10.10.102]

RUNNING HANDLER [strongswan : Restart the strongswan service] ********************************************************************************************************************************
changed: [10.10.10.102]
changed: [10.10.10.101]

PLAY RECAP ***********************************************************************************************************************************************************************************
10.10.10.101               : ok=11   changed=9    unreachable=0    failed=0   
10.10.10.102               : ok=11   changed=9    unreachable=0    failed=0   

```
## Vagrantfiles for testing stuff out

I am running KVM so most of this will be tuned for libivirt. This project is in it's first phase, so it kinda empty now.

You might want to manually add the mgmt-net.xml to libvirtd since there is a bug now on vagrant and it will fail to create the network. If you want your VMs to have internet on the default net, use the default instead of mgmt-net.
